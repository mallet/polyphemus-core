# Copyright (C) 2008, ENPC - INRIA - EDF R&D
#
# This file is part of the air quality modeling system Polyphemus.
#
# Polyphemus is developed in the INRIA - ENPC joint project-team CLIME and in
# the ENPC - EDF R&D joint laboratory CEREA.
#
# Polyphemus is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your option)
# any later version.
#
# Polyphemus is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# For more information, visit the Polyphemus web site:
#      http://cerea.enpc.fr/polyphemus/

import subprocess
import sys
SetOption('warn', 'no-duplicate-environment')

# Put the path to Polyphemus.
# Also editable from command line with option "polyphemus".
polyphemus_path = "../../"

execfile(polyphemus_path + "include/common/SConstruct_include")

executable_suffix = []

# The command-line option "chemistry" must belong to chemistry_list.
create_variable("chemistry_list", None)
chemistry_list = ["racm", "racm2", "cb05"]

if "chemistry" in ARGUMENTS:
    if not ARGUMENTS["chemistry"] in chemistry_list:
        raise Exception, "Invalid value for option \"chemistry\": it should" \
              " be \"racm\", \"racm2\" or \"cb05\"."
    else:
        chemistry = ARGUMENTS["chemistry"]
        executable_suffix.append(chemistry)
else:
    nacl_args = ["yes", "no"]
    if "nacl" in ARGUMENTS:
        nacl_args = [""]
    status = 0
    args = ["", ""]
    for nacl in nacl_args:
        if nacl:
            args[0]="nacl=" + nacl
        for chemistry in chemistry_list:
            args[1]="chemistry=" + chemistry
            cmd = sys.argv + args
            print "=> Building for: ", " ".join(args)
            if subprocess.call(cmd, shell=False) != 0:
                status = 1
    sys.exit(status)

# Include paths: put the absolute paths, the relative paths or the paths
# into the Polyphemus directory.
include_path = """
include/common
include/Talos
include/SeldonData
include/AtmoData
include/isorropia_aec
include/fastJX
include/modules/common
include/modules/transport
include/modules/transport/SplitAdvectionDST3
include/modules/transport/DiffusionROS2
include/modules/aerosol
include/modules/aerosol/Aerosol_SIREAM_AEC
include/modules/aerosol/Aerosol_SIREAM_AEC/INC
include/modules/aerosol/Aerosol_SIREAM_AEC/CHEMISTRY/racm
include/modules/aerosol/Aerosol_SIREAM_AEC/CHEMISTRY/common
include/modules/aerosol/Aerosol_SIREAM_AEC/AEC
include/modules/aerosol/Aerosol_SIREAM_AEC/SIMPLE_AQUEOUS
include/modules/aerosol/Aerosol_SIREAM_AEC/VSRM
include/modules/aerosol/Aerosol_SIREAM_AEC/SIREAM
include/models
include/driver/common
include/driver/common/output_saver
""".replace("racm", chemistry)

# [Optional] Compiled library paths: put absolute paths, relative paths or
# paths that are in the Polyphemus directory. Put the list of paths to
# compiled libraries that may not be installed in your system root.
library_path = None

exclude_dependency = ["typeaoutput\.c", "typeboutput\.c", "read_shm\.c",
                      "set_shm\.c", "write_shm\.c", "read_shm_aer\.c",
                      "write_shm_aer\.c" ]

# Hack in Polair3DChemistry.
flag_cpp = "-DPOLYPHEMUS_WITH_AEROSOL_MODULE"

# With or without (default) NaCl in thermodynamics.
ARGUMENTS["nacl"] = ARGUMENTS.get("nacl", "no")
if ARGUMENTS["nacl"] == "no":
    flag_cpp += " -DWITHOUT_NACL_IN_THERMODYNAMICS"
    flag_fortran = " -DWITHOUT_NACL_IN_THERMODYNAMICS"
elif ARGUMENTS["nacl"] == "yes":
    executable_suffix.append("nacl")
else:
    raise Exception, "Invalid value for option \"nacl\": it should be " \
          + "\"yes\" or \"no\"."

execfile(polyphemus_path + "/include/common/SConstruct_main")

# Informs SCons on the targets to be built. It is assumed that all targets
# have the same dependencies.
for filename in target_list:
    env.Program(filename[:-4] + '-' + '-'.join(executable_suffix),
                [filename] + dependency_list)

