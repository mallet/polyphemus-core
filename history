o--------------------o
| POLYPHEMUS HISTORY |
o--------------------o


Version 1.9 (2015-04-20)
-----------

** Preprocessing

- Added MEGAN biogenic emission preprocessing with the "bio-megan" program.
- Added support for GLC 2000 in "luc-glcf".
- Added the lattitude/longitude projection to the WRF meteo processing.
- Added the FastJX photolysis rates scheme in the WRF and MM5 meteo processing.
- Added the computation of the cloud top height, renaming the former cloud height
  to cloud base height.

** Aerosol

- Implemented support for organic aerosol formation in Siream-AEC.
- Added new biogenic precursors and explicitly NOx regime in Siream-SORGAM.
- Added experimental support for the FastJX photolysis rates scheme in
  Siream-SORGAM.
- Included the Spack library, with automatic generation of chemical mechanism
  by SCons.
- Updated RACM, RAMC2 and CB05 mechanism.
- Updated Isorropia patches to support OpenMP parallelism (work in progress).

** Models

- Added lineic Gaussian modelisation for road traffic simulations:
     + implemented the discretized lineic Gaussian sources,
     + made it compatible with the "plume in grid" model for multi-scale
       simulations,
     + parallelized the "plume in grid" model for line sources using MPI.

- Improved scavenging computations:
     + added Belot parameterization for in-cloud scavenging,
     + split below-cloud and in-cloud scavenging so that both may be applied
       at the same time,
     + took into account the depth of the clouds.

** Data assimilation

- Greatly improved data assimilation and observation management, using the
  Verdandi library.

** Bug fixes

- Numerous bug fixes, among which:
     + in MPI parallelization, MPI_init was not always called,
     + numerous fixes in scavenging computations,
     + when using 'Polair3DAerosol' with 'Decay' volume emissions for
       particulate species were added twice,
     + when reading LUC input data from USGS, abscissa and ordinate axis were
       inverted,
     + negative concentrations could be encountered with the programs "plume"
       and "plume_aer" when negative wind angles were used for the line
       sources,
     + in the Gaussian plume model, combination factor was reset for line
       sources after the discretization.

** Development tools

- Made the source code compatible with newer gcc versions, up to gcc-5 witout
  OpenMP, and gcc-4.8 when using OpenMP.
  

Version 1.8.1 (2010-12-02)
-------------

** Bug fixes

- AtmoPy could not be imported anymore because of an indentation error.


Version 1.8 (2010-11-16)
-----------


** Modules

- Overhauled the gazeous chemistry modules, added the CB05 and RACM2
  mechanisms and updated the RACM species data: the module
  'Photochemistry' replaces and extends the former 'ChemistryRACM' module.
  It can use the chemical mechanisms RACM, RACM2 and CB05. In the related
  configuration files, the field 'option_chemistry' has been added with three
  possible values 'RACM', 'RACM2' and 'CB05'. The field 'Photolysis_tabulation_option'
  has also been added and offer the choice to compute the photolysis rates
  from tabulations or read them from binary files.

** Aerosol

- Overhauled the aerosol modules to add the chemical mechanisms RACM2 and
  CB05. Species data have also been updated for the RACM mechanism.
- In the 'SIMPLE_AQUEOUS' model of the aerosol modules, added the same
  redistribution process as in the 'VSRM' model.
- Improved the stability of the aerosol thermodynamics module ISORROPIA.

** Gaussian models

- In the model 'GaussianPlume', improved the line source model:
     + added corrective terms for receptors that are downwind the source and
       at extremities of the source when the source stability class is 'D',
     + modified the threshold for the line source / discretized source
       combination,
     + added the line source correction for the different stability classes,
     + improved the initialization of the standard deviation.

** Preprocessing

- Added species data files related to the chemical mechanisms RACM2 and CB05
  and used in preprocessing programs. Also updated the same species data for
  the chemical mechanisms RACM.
- Improved "luc-usgs" to handle more precisely output grids whose resolution
  are as high as the input grid of LUC.
- Modified the "discretization" program: segments of a broken line are now
  modelled with half source points at their vertices and extremities.

** Bug fixes

- Negative concentrations could be encountered with the programs "plume" and
  "plume_aer" when negative wind angles were used for the line sources.
- With the driver 'PlumeDriver', when "coordinates_list" was the first
  saver unit type, others saver units with a different type were wrongly
  managed.
- In the library 'AtmoData', the total number of LUC cells at the boundary of
  the computational domain was erroneous in 'GridCorrespondences'.


Version 1.7.4 (2010-06-16)
-------------

** Bug fixes

- The initial and final times provided to the gaseous chemistry solvers of
  'ChemistryRACM', 'ChemistryRADM', 'AerosolRACM_SIREAM' and
  'AerosolRACM_SIREAM_AEC' could be improperly modified when using the
  default photolysis rates because a related temporary variable was not
  systematically initialized.


Version 1.7.3 (2010-06-04)
-------------

** Bug fixes

- Fixed the same bug as in version 1.7.2 but within the modules
  'AerosolRACM_SIREAM' and 'AerosolRACM_SIREAM_AEC'.


Version 1.7.2 (2010-05-31)
-------------

** Bug fixes

- The initial and final times provided to the gaseous chemistry solvers of
  'ChemistryRACM' and 'ChemistryRADM' could be improperly modified during the
  computation of the zenithal angles when using the default photolysis rates.


Version 1.7.1 (2010-05-27)
-------------

** Bug fixes

- In the module 'AerosolRACM_SIREAM_AEC', some UNIFAC parameters were
  erroneously initialized.


Version 1.7 (2010-05-07)
-----------

** General

- Improved the management of plume rise parameters for point sources. In the
  configuration files for point sources, 'Diameter' replaces 'Section'.
- Extended the parallelization to:
  	+ the module 'ChemistryRADM' with MPI and OpenMP,
	+ the driver 'OptimalInterpolationDriver' with MPI.

** Gaussian models

- In the model 'GaussianPlume', added the possibility to have an initial sigma
  for line sources.
- In the model 'GaussianPuff', simplified the computation of loss processes,
  in particular dry deposition with Chamberlain model.
- Added the reflection factor for the model 'PuffChemistry', to allow
  computation of the dry deposition with the Overcamp model in case there is
  in-puff chemistry.

** Aerosol

- Updated UNIFAC parameters and chemical surrogates representation.
- Improved the computational efficiency of the module
  'AerosolRACM_SIREAM_AEC'.

** Driver

- In the driver 'MonteCarloDriver', added:
     	+ support for a model parallelized with MPI,
	+ the ability to save in a file the generated random numbers,
	+ support for a perturbation of the wind angle.

** Preprocessing

- Modified the program 'discretization' to have half sources at extremities of
  a segment.

** Plume-in-grid model

- Added the loss processes.

** Bug fixes

- The modules 'ChemistryRACM', 'ChemistryRADM', 'AerosolRACM_SIREAM' and
  'AerosolRACM_SIREAM_AEC' failed to integrate over a time step whose initial
  and final dates were in two different years.
- In the module 'AerosolRACM_SIREAM_AEC', the configuration file section
  '[saturation_pressure_mass]' could be misread.
- In the library 'AtmoData':
	+ an error in the function 'LonLatToWRFLccInd' resulted in an offset
 	  in the Lcc projection of 'WRF-meteo'.
        + the function 'ComputeAttenuation_LWC' miscalculated the attenuation
	  when clouds reached the upper model level.
- In the model 'GaussianPlume', when several line sources were used, the line
  source and the discretized source were not working when combined together.
- Memory leak problems could arise with the 'GaussianPuff' model.


Version 1.6 (2009-11-13)
-----------


** General

- Overhauled the parallelization:
	+ the MPI parallelization of the modules 'ChemistryRACM' and
	'SplitAdvectionDST3' was simplified and the modules 'DiffusionROS2',
	'AerosolRACM_SIREAM' and AerosolRACM_SIREAM_AEC' have been
	parallelized with MPI,
	+ the modules 'ChemistryRACM', 'SplitAdvectionDST3', 'DiffusionROS2',
	'AerosolRACM_SIREAM' and AerosolRACM_SIREAM_AEC' have also been
	parallelized with the shared memory parallel model of OpenMP,
	+ patches of the external module 'ISORROPIA' have been added to comply
        with the OpenMP parallelization of 'AerosolRACM_SIREAM' and
	AerosolRACM_SIREAM_AEC'.
- Added two SCons options for the command line:
	+ to set up the C compiler, and
	+ to compile with the OpenMP and/or the MPI parallelization library.

** Preprocessing

- Added support for WRF, with "meteo/WRF-meteo.cpp".

** Lagrangian stochastic models

- Added the model 'LagrangianTransport' to solve stochastically the
  advection-diffusion of gaussian kernel particles. For the particles,
  'Horker' and 'Fokker-Planck' formulations have been implemented.

** Gaussian models

- Added support for continuous line sources in the model 'GaussianPlume'.
- Modified the program "discretization.cpp":
	+ Now, several independant segments can be discretized, in addition to
	a single broken line.
	+ The emission rate (or quantity) must now be specified per unit of
	length, in mass/s/m (resp. mass/m).
	+ The number of points to discretize each segment now includes the
	segment nodes.

** Aerosol

- For the module 'AerosolRACM_SIREAM', added a patch for ISORROPIA 1.7
  (2009-05-27) to be applied if the OpenMP parallelization is used.
- For the module 'AerosolRACM_SIREAM_AEC', added a new patch for the last
  release of ISORROPIA 1.7 (2009-05-27).
- For the modules 'AerosolRACM_SIREAM' and 'AerosolRACM_SIREAM_AEC', added the
  display option "Show_configuration".

** Bug fixes

- When compiling a program using an aerosol module, the SCons command line
  option "nacl" was implicitly always set to "yes".
- In the module 'SplitAdvectionDST3', the boundary conditions on the lowest
  and highest levels were not correctly assigned.
- In the preprocessing program MM5-meteo, results were shifted with an offset
  of 0.5 cell.
- The programs polair3d-decay, polair3d-siream, polair3d-siream-aec and
  puff_aer could encounter a segmentation fault when compiled with recent
  versions of GNU GCC (posterior to 4.3).
- In the simple aqueous submodule of the module 'AerosolRACM_SIREAM', the
  sulfate concentrations were wrongly computed because of a sign error.
- The program compute_tab that computes the Mie tabulation for the
  postprocessing program optics could abort with a segmentation fault.
- In the model 'Polair3DAerosol', some configuration checkings were not
  performed.
- In the module 'AerosolRACM_SIREAM', 'option_adaptive_time_step' was not
  always explicitly initialized.


Version 1.5 (2009-05-20)
-----------


** General

- Added the Python module 'ensemble_generation'.

** Preprocessing

- "meteo" and "attenuation" were not able to treat properly ECMWF data created
  after 2006-02-02. The size of the table containing the hybrid coefficients
  was indeed supposed to be always equal to 61. Now, this size is determined
  directly from the actual file "hybrid_coefficients.dat".
- Introduced a new parameterization to compute the critical relative humidity
  in the program "attenuation".
- In the programs "gaussian_deposition" and "gaussian_deposition_aer", added
  an option "file" to read the coefficients and deposition velocities in a
  file with all meteorological situations (examples are "deposition.dat" and
  "scavenging.dat"). Also added a keyword "Velocity_part" in the configuration
  file, to mention whether the diffusive or total part of the deposition
  velocity is given. In the case it is "diffusive", the gravitational settling
  velocity is computed.
- Modified the programs "discretization" and "discretization_aer" to match the
  changes in the point sources management: now there can be a list of species,
  along with a list of corresponding rates or quantities.
- Added the ability to multiply the boundary layer height by a constant in the
  Troen & Mahrt parameterization.

** Driver

- Modified the perturbation scheme so that fields with two values per step
  ('_i' and '_f') are properly handled in 'AssimilationDriver',
  'PerturbationManager' and 'MonteCarloDriver'.
- In the drivers 'EnKFDriver', 'RRSQRTDriver' and 'MonteCarloDriver',
  introduced a more complete re-initialization before any call to the model's
  integration over one time step. This may be needed for the model to behave
  properly.

** Gaussian models

- Added an option for subcycling of chemistry in 'GaussianPuff'.
- Added an option to save the total plume mass in a binary file.

** Plume-in-grid model

- Added an alternative method to take into account background concentrations
  for puff chemistry with the plume-in-grid model
  (option "With_chemistry_feedback").
- Added the possibility to put "rural" or "urban" instead of providing a LUC
  file in the plume-in-grid model.

** Other models

- Added the option of adaptive time stepping for RACM, RACM-SIREAM and
  RACM-SIREAM-AEC gaseous chemistry.
- In 'Polair3DTransport', 'Polair3DChemistry' and 'Polair3DAerosol', moved the
  computation and the transformation of a few fields from 'InitStep' to
  'Forward' so that more raw input fields may be perturbed.
- The aerosol module 'AerosolRACM_SIREAM' now supports parallel computing.
  Corrected and updated parallel computing for the aerosol module
  'AerosolRACM_SIREAM_AEC'.
- In the aerosol modules, the increments of bulk concentrations computed by
  VSRM are now distributed among sections proportionally to the initial
  particle distribution.

** Postprocessing

- Initial import of class 'Source' and several related functions useful to
  display point sources in AtmoPy.
- Added functions 'fac2', 'fac5', 'nmse_1', 'mg', 'vg' and 'fmt'. Renamed
  'nmse' to 'rnmse_2' in AtmoPy.

** Bug fixes

- In the aerosol module 'AerosolRACM_SIREAM', the redistribution of
  concentrations in the sections after the aqueous phase computation was not
  correct.
- Significant differences in the results of the parallelized and serialized
  versions of programs including the model 'Polair3DChemistry' were
  encountered when the source splitting method was used. In the parallelized
  version, data useful to this method were not scattered properly throughout
  the parallel processes.
- 'InCloudWetDepositionFlux' and 'InCloudWetDepositionFlux_aer' were badly
  taken into account when parallel computing was used in the aerosol modules.
- When parallelized jobs of "polair3d-siream" or "polair3d-siream-aec" were
  terminated abnormally, shared memory segments were not properly deleted.
  Then, the user was not able to relaunch a similar job on the same machine.
  System ressources were also wasted.
- In the Gaussian puff model, when there was plume rise with the puff above
  the boundary layer, the standard deviations were computed at the wrong
  height.
- In the plume-in-grid model, the puff transfer was wrongly done when the puff
  was above the boundary layer.
- "optics" failed to read properly the OPAC files when they were preprocessed
  as indicated the user's guide, i.e. when '#' was replaced by ''.
- In the plume-in-grid model, puffs reaching the end of the simulation domain
  were improperly handled.
- WGRIB was requested in order to compile any program in
  "preprocessing/meteo/" although only "attenuation" and "meteo" need it.


Version 1.4 (2008-11-20)
-----------

** General

- Replaced the directory "driver/" with:
	+ the directory "include/driver/" where the drivers are now located,
	and
	+ the directory "processing/" where several examples are given
	  with their configuration files.
- Wrote a new support for SCons with improved portability and many options.
- Implemented constraints on the values read in the configuration file(s).
- Renamed the makefiles of Polyphemus to indicate the related compiler.
- Added "utils/check_observation".
- Improved the script "utils/format" so that the formatted files are no more
  overwritten in case no change was made in them.

** Gaussian models

- Added support of plume rise in the Gaussian puff model.
- Added chemistry for gaseous species in the Gaussian puff model (RACM
  mechanism).
- Added more plume rise formulae to use in the Gaussian plume/puff model.
- Added an alternative formula to compute the plume/puff standard deviations
  above the boundary layer.
- Added an alternative formula to compute the plume/puff standard deviations
  for elevated sources, in the case similarity theory is used.
- Gaussian models now use the point emission manager common to all models
  for gaseous species. The source configuration files slightly change. The
  Gaussian puff model automatically discretizes a continuous source into a
  series of puff, with a specified time step.
- Added the option "spatial_average" for the saver type "coordinates_list":
  it allows to save the averaged value over a given volume around the point
  instead of the exact concentration. This is relevant for Gaussian and
  plume-in-grid models.

** Other models

- Added chemistry for gaseous species in the plume-in-grid model (RACM
  mechanism).
- In the plume-in-grid model, the mass transfered in a cell is the exact puff
  integral over the cell volume instead of an approximation of it.
- In the plume-in-grid model, added a reading of LUC file in order to
  determine whether to use the urban or rural formulae when using the Briggs
  parameterization.
- Added a new type of point emissions: temporal emissions. They are like
  continuous emissions, but with temporal factors that are read in a binary
  file and applied to the emission rate every given time step.

** Drivers

- Added support for parallel computing in 'BaseDriver'.
- Added 'PlumeMonteCarloDriver' for Monte Carlo simulations with a Gaussian
  plume model.
  An example called "uncertainty-plume" is provided in "processing/gaussian/".
- Added 'PerturbationDriver' that allows to perturb input fields in a model.
- Strongly sped up 'OptimalInterpolationDriver'.
- In 'OptimalInterpolationDriver', added the ability to produce an analysis
  before the first simulation step (that is, with the initial conditions).

** Modules

- 'SplitAdvectionDST3' and 'ChemistryRACM' now support parallel computing.

** Preprocessing

- Corrected weekdays factors in a data file used by "emissions".

** Bug fixes

- In 'SplitAdvectionDST3::Forward': the number of subcycles along z was taken
  as that along y.
- In the computation of the Lagrangian vertical time scale, the input
  parameters where taken in the wrong order in 'ComputeVerticalSigma'.
- The model behaved as if the photolysis data contained one level less than
  it actually has. In other words, the top altitude in the photolysis input
  data was simply ignored.
- In 'GaussianPlume::InitMeteo', the inversion height was determined with an
  uninitialized stability class.
- In 'GroundObservationManager::ReadStation', the paths to the station files
  could include unwanted trailing spaces.
- In 'SaverUnitDomain_assimilation::Init', the initial date could not be
  written in the "date file", even if an analysis was saved at that date.


Version 1.3.1 (2008-05-30)
-------------

** Bug fixes

- The puff source treatment of version 1.3 required new outputs from the
  program "discretization".
- Fixed some bugs in the "SConstruct" files.


Version 1.3 (2008-05-19)
-----------

** Postprocessing

- Added the program "optics" that computes several aerosol optical parameters
  from the results of a chemistry-transport model.
- Added examples for ensemble forecasting (loading ensemble simulations,
  performing sequential aggregation).

** Aerosol

- In the aerosol module, added a new scheme for modeling secondary organic
  aerosols based on the AEC model (California Air Resources Board). The
  resulting SIREAM-AEC module uses a modified isorropia for which a patch is
  provided. Also, developed a parallel computing support for it (based on
  forks).

** Preprocessing

- In programs "Kz", "Kz_TM" and "MM5-meteo", introduced a specific value for
  the minimum of the vertical diffusion coefficient over urban areas.
- In programs "bc", "bc-dates" and "ic", extended the interpolation domain
  along longitude so that domains containing -180 degrees or 180 degrees are
  supported.
- Updated file "time_zones.dat" to take into account countries that were added
  to EMEP database.
- Removed the now useless program "Polair3D-bc".

** All models

- Improved the way point emissions to be released are described. Now all
  models can easily handle continuous and puff emission sources.
- Set 'SplitAdvectionDST3' as the default Eulerian advection module
  (except in assimilation-related examples), in the place of 'AdvectionDST3'.
  This means that the Courant-Friedrichs-Lewy condition will be automatically
  enforced. As a consequence,removed "polair3d-split.cpp" and replaced it with
  "polair3d-unsplit.cpp". Note that "polair3d.cpp" is now the same as the old
  "polair3d-split.cpp".

** Gaussian models

- Modified computation of the concentrations: the formulae are different if
  the plume is above or below the inversion layer.
- Added the partial penetration of the plume in the inversion layer: if there
  is plume rise, the plume can split in two parts, above and below the
  inversion layer.
- Modified the way the inversion height is specified: the boundary height is
  now always read, and the inversion height is equal to boundary height
  during daytime and to zero during nighttime.
- Added an initial spread due to the source size.
- Added an initial plume spread due to plume rise.
- Modified computation of the concentrations: added a far field model applied
  below the inversion layer.
- Completed the implementation of Briggs formulae for plume rise.
- Added a minimal value for the crosswind wind standard deviation: this avoids
  to have zero values for standard deviations when the plume is at the
  inversion layer height. Used only in stable cases.
- Added one more reflection due to the inversion layer in the plume
  concentration computation.
- Stability classes are computed if necessary according to the Monin-Obukhov
  length.

** Other models

- In plume in grid, added another injection method: the puff may be injected
  in all surrounding cells, averaging the concentration over the cell volume,
  instead of injecting in one column.
- In Polair3DTransport, added a parameterization to compute scavenging
  coefficient for radionuclides (Pudykiewicz).

** Other changes

- Added a Polyphemus-wide support of SCons.
- Added the utilities "apply_on_files", "replace_string" and "format".
- In 'GroundObservationManager', switched from the EMEP station description
  format to the standard description format.

** Bug fixes

- In Polair3DAerosol, particle volume emissions were not taken into account
  when a cloud was diagnosed.
- In programs "bc", "bc-dates" and "ic", temperature and surface pressure were
  improperly shifted by 180 degrees along the longitude.
- In the aerosol version of the Gaussian plume model, only dispersion was
  performed, without loss processes.
- The computation of standard deviations with Doury formulae in the case of
  low dispersion was wrong.
- In Polair3DAerosol, fluxes from in-cloud scavenging were not collected
  for gaseous species when the simple aqueous model was used.
- In Polair3DTransport, the point emissions were slightly modified because
  the cell volumes were badly computed.


Version 1.2.2 (2007-12-20)
-------------

** Bug fixes

- In "MM5-meteo.cpp", the limits of the domain were badly computed which led
  to errors in the wind components and the wind module.
- In the plume in grid model, the volume of Eulerian cells could be badly
  computed.
- Aerosol volume emissions were not taken into account when aqueous chemistry
  was called.
- An error occurred when the time step for the preprocessing and the
  simulation were not the same in "water_plume.cpp".

** Other changes

- 'StationaryDriver' is now a model, called 'StationaryModel'.
- A convergence criterion has been added to 'StationaryModel'.
- The algorithm used to compute liquid water content in "water_plume.cpp" has
  been improved.
- It is now possible to choose the output unit of "water_plume.cpp" (g/kg or
  g/m^3) and to save the water content in the plume only (instead of total
  liquid water content).


Version 1.2.1 (2007-11-05)
-------------

** Bug fixes

- Corrected a bug in the test to detect plume reflections on the ground or
  inversion layer.
- A segmentation fault occurred when some options needed by 'EnKFDriver' were
  missing in the configuration files.
- Gravitational settling was not taken into account by the simple aqueous
  module.
- The wind module used to generate sea-salt emissions when meteorological raw
  data comes from MM5 was not the correct one.


Version 1.2 (2007-10-18)
-----------

** Plume in grid

- Added possibility to choose a time criterion to inject the puff into
  the Eulerian model, as well as a size criterion.
- Changed puff transfer method: it is now transferred into several vertical
  levels (below the inversion layer) instead of a single cell.
- Added possibility to use similarity theory in plume in grid model.
- Added possibility to use independent meteorological data for the different
  puffs (taken in the cell where the puff center is), or to interpolate it at
  the puff center.
- Plume in grid is now a model and not a driver anymore.

** Gaussian models

- Added possibility to use similarity theory to compute the standard
  deviations in the Gaussian puff model (it was only in Gaussian plume model
  before).
- Improved treatment of puff/plume reflection:
      + Reflection on ground and inversion layer only occurs when puff touches
        respectively the ground or the inversion layer.
      + When boundary layer height is known, inversion height is equal to
        boundary layer height if it is daytime and to zero if it is nighttime.
      + There is only one reflection on the inversion layer (as on the
        ground), not five as before.
- Added separated formulae to compute downwind horizontal dispersion parameter
  (sigma_x) with similarity theory for Gaussian puff model. It used to be
  considered equal to crosswind parameter (sigma_y), as for Briggs and Doury
  formulae.
- Added ability to force sigma coefficients to be increasing in case
  meteorological data are unstationary in puff model (useful only when using
  the puff model with plume-in-grid).

** Modules

- Added simple aqueous module for aerosols.
- Added EQSAM as an alternative to Isorropia for thermodynamics in the bulk
  equilibrium configuration of aerosol.
- Added a test to prevent out of range values in case the number of bins used
  for aerosol volume emission differs from species to species.
- In 'DiffusionROS2', vertical diffusion is now processed even if vertical
  diffusion coefficient is equal to zero. This allows to take into account
  ground boundary condition in this case.

** Data assimilation

- Added support for 2D layers of synthetic observations (simulation option
  'with_level').
- Added the possibility to remove the background term in the cost function.
- Added option 'With_positivity_requirement' for all sequential data
  assimilation algorithms.
- Added support of Balgovind background error covariance for 4DVar.
- Parameters of Balgovind error parameterization are now species-dependent.

** Preprocessing

- Added the option to use the parameterization of Smith and Harrison
  (1998) to generate sea salt.
- Added "bc-dates.cpp" to compute boundary conditions over a range of dates.
- Modified "bc-gocart.cpp" so that the number of days for which boundary
  conditions are computed is now given as an argument.
- Improved error message displayed when a MM5 file is not found.

** Bug fixes

- In Gaussian models, deposition could occur on the inversion layer.
- Option "all" in saver configuration file, used to save concentrations for
  all species, failed for aerosol Gaussian models.
- Program "plume_aer" failed when the number of aerosol species was smaller
  than the number of gaseous species and decay was used.
- Corrected slight bugs in computation of lagrangian horizontal time scale and
  horizontal wind standard deviation for neutral cases (used to compute
  standard deviations with similarity theory).

** Other changes

- Modified the library used to decode GRIB files for portability reasons.
  Program "meteo" now works also on 64-bit architectures.
- Modified "get_info_MM5.cpp" so that it works on 64-bit architectures.
- Added the Python module "polyphemus.py" to launch and manage Polyphemus
  programs.
- Saver of type 'coordinates_list' can now be used with Polair3D models.


Version 1.1.1 (2007-06-21)
-------------

** Bug fixes

- The current date was badly updated inside the time loop for several
  preprocessing programs.
- In 'EnKFDriver', 'RRSQRTDriver' and 'MonteCarloDriver' fields were not
  properly perturbed (minor bug).
- The test to check that a given date is on a Mozart file was wrong. When the
  test failed, no data was generated.
- In 'FourDimVarDriver', the output saver was initialized too early, hence it
  did not save the optimized initial concentrations.
- Variances for observation errors could only be an integer.
- 'Optimizer' could be deleted even if it was not previously allocated (minor
  bug).
- Changed the saving format for backup saver units to 'float' (only format
  that can be used for initial conditions).
- The test on cloud height basis used to compute scavenging in 'Polair3D'
  models was badly written.

** Other changes

- For 'water_plume.cpp', a beginning and an end date are now given in command
  line, as it was already the case for most preprocessing programs.
- Improved the management of EMEP inventories for 'distribution.cpp'.
- In ensemble Kalman filter, the first value used for prediction step is now
  the mean of all ensemble members.


Version 1.1 (2007-05-09)
-----------

** Data assimilation

- Added ensemble Kalman filter ('EnKFDriver').
- Added reduced-rank square root Kalman filter ('RRSQRTDriver').
- Added 'Polair3DChemistry' adjoint model with respect to concentrations
  ('Polair3DChemistryAssimConc').
- Added drivers for the validation of adjoint models ('AdjointDriver',
  'GradientDriver' and 'Gradient4DVarDriver').
- Added 4D-Var driver ('FourDimVarDriver').
- Added support for one error variance per species.
- Added a vertical correlation in Balgovind covariance function.
- The random generator may now be initialized with a constant seed, with the
  computer time or with the seed of the Newran seed directory.
- Speeded up 'GroundObservationManager::SetDate'.
- Added support of spatial interpolation for 'GroundObservationManager'.

** Drivers

- Added a plume in grid driver ('PlumeInGridDriver').
- Added a Monte Carlo driver ('MonteCarloDriver').
- Added support for SCons in "driver/".
- Added an example for RADM in "driver/example/radm/".
- Added an example for a simulation at global scale in
  "driver/example/global/".

** Models

- Added Doury parameterization for standard deviation in Gaussian plume and
  puff models.
- Added parameterization for standard deviation based on similarity theory
  for Gaussian plume model.
- Added support for additional surface emissions in 'Polair3DTransport'.
- Enabled the flag 'POLYPHEMUS_WITH_AEROSOL_MODULE' to ease the compilation.
- Improved 'CastorTransport' computing performances.
- Improved miscellaneous error management and messages.
- Castor concentrations may now be in several units: mass (microgramm/m^3),
  ratio (ppbv) or number (molecules/cm^3).

** Savers

- Added the ability to collect dry deposition fluxes and wet deposition
  fluxes (including in-cloud deposition fluxes), for gaseous and aerosol
  species.
- Added class 'SaverUnitDomain_prediction' to save the prediction results
  after the assimilation period.
- Added the ability to save concentrations of ensemble members (possibly
  analyzed), with saver types 'domain_ensemble_forecast' and
  'domain_ensemble_analysis'.
- Added a saver unit to save concentrations at given points.
- Added a saver unit to save a backup of concentrations in order to restart
  a simulation which would have been aborted.

** Modules

- Added transport modules 'GlobalAdvectionDST3' and 'GlobalDiffusionROS2' for
  transport at global scale.
- Added transport module 'SplitAdvectionDST3' which implements an advection
  scheme with directional splitting and with automatic subcycling in order to
  satisfy the CFL.
- Speeded up 'ChemistryCastor' computations by a factor of 4 or 5.
- Thresholded concentrations in 'Decay' so that they do not become negative.
- Added 'BaseModule' from which all modules ('transport', 'chemistry' and
  'aerosol') derive.

** Postprocessing

- Moved from Numarray to NumPy.
- Modified water plume diagnosis so that it is done for each time step and not
  only the last one.

** Preprocessing

- Improved the management of dates for preprocessing programs. Now a
  beginning and end dates are given for most preprocessing programs, which
  allows to use the program for more than one day.
- Added program "extract-glcf.cpp" to compute the land-use file needed by
  "emissions.cpp".
- Updated "time_zones.dat" (input file of "emissions.cpp") to be consistent
  with EMEP inventories.
- Improved the management of EMEP inventories.
- Added a program to generate NH4 boundary conditions from Gocart data.
- Added support for INCA boundary conditions (for Castor model).
- Added support for Melchior chemical mechanism at emission stage.
- Emissions for Castor may now be generated without Chimere emission files,
  that is, starting from EMEP inventories.
- Added "ground-castor.py" to generate land use from Chimere data files.
- Improved Castor preprocessing.
- Improved the numerical accuracy of boundary conditions generated from Gocart
  data.
- Improved configuration files reading.

** Bug fixes

- In "init_aerosol.py", a bin was not taken into account in the computation
  of PM2.5.
- "MM5-meteo-castor" crashed when the meteorological and simulation time steps
  were different.
- SIREAM could crash because the rain field may not be initialized.
- 'sol_date' was badly initialized in "bio-castor.cpp" and "dep-emberson.cpp".
- The number of aerosol species was defined but not initialized when 'Decay'
  module was used with Polair3DChemistry.

** Other changes

- Improved the management of the second underscore appended to given Fortran
  external identifiers; introduced 'POLYPHEMUS_SINGLE_UNDERSCORE' and
  'POLYPHEMUS_DOUBLE_UNDERSCORE'.
- Added eight new utilities in "utils/".


Version 1.0.1 (2006-12-14)
-------------

** Bug fixes

- In 'GaussianPuff', the current date was not reinitialized for each
  meteorological situation, therefore all situations were not saved.


Version 1.0 (2006-10-11)
-----------

Since version 0.2 of Polyphemus, a lot of work has been achieved. Below is
just a list of main highlights:
- Strongly modified Polair3D (ergonomy, C++ framework).
- Added chemistry-transport model Castor (clone of Chimere -- except for
  aerosols).
- Added Gaussian models.
- Added aerosol support (primarily with module SIREAM).
- Added drivers for optimal interpolation and local scale simulations.
- The directory "programs/" used for preprocessing is now called
  "preprocessing/".
- Added postprocessing abilities (model evaluation, water diagnosis).
- Added interactive postprocessing tool AtmoPy.


Version 0.2 (2004-10-13)
-----------


Version 0.1 (2004-07-30)
-----------
